---
tags: eole3
--- 

Dépôt source du helm permettant l'installation de laboite sur un cluster Kubernetes

Pour l'installation utiliser le dépôt [tools][1].

[1]: https://gitlab.mim-libre.fr/EOLE/eole-3/tools/
