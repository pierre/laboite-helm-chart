# Changelog

## [1.3.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.2.0...release/1.3.0) (2023-04-18)


### Features

* publish stable version for laboite 5.3.0 ([06cf408](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/06cf408281d43fd0e2b095ebd20a356b0749162e))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/compare/release/1.1.0...release/1.2.0) (2023-02-01)


### Features

* publish laboite 5.2.0 ([3162378](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/31623783de729f6d6523a4a608974d0f11719ed8))


### Bug Fixes

* chart version must be dev on dev branch ([b9bc328](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/b9bc3285e90286d29920c53470f1411eabb81544))
* dev chart version deploy dev app version ([0ec545c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/0ec545ca87779b4e1f9d2a371effa4e2fb4e72e6))
* helm testing deploy testing app version ([d6c546b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/d6c546b8202ee474929abddb1e80980f164d07cc))
* remove mongodb backup ([06d8aa7](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/06d8aa7fea667031890d2910c5a4570989b03299))
* remove useless traefik files ([c972e39](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/c972e3992c28a3dc1950b7962bb0aace2d8149bd))
* update leblog and lookup-server chart version ([086752e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/086752ef3319a245270aa62271fde51aa3336b3e))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/compare/release/1.0.0...release/1.1.0) (2022-12-12)


### Features

* update subcharts dependencies ([78c5d2e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/78c5d2e52997b95dc23ce7a73de9294ccde3a1e6))

## 1.0.0 (2022-12-12)


### Features

* **chart:** update dependencies to use `dev` helm ([cd4d7e9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/cd4d7e956e542672d5b05a19b457b00df681b60e))
* **ci:** build helm package ([f48e598](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/f48e598cf4ffa467bc48e554fda126698a2bcca0))
* **ci:** push helm package to chart repository ([9d56470](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/9d56470d61a34fa8165e5df08db8e3336e8e9263))
* **ci:** update helm chart version with `semantic-release` ([a0e74d6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/a0e74d67951da3cca222e7374fef5f5decd67035))
* **ci:** validate helm chart at `lint` stage ([46fff92](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/46fff924e0170ef81b00090c6fec2bc764e27682))
* content of meteor-settings secret is in values file ([0833546](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/083354654684da4e8dd301141a8056f2c78aabd0))
* **laboite:** add ui env parameter in meteor settings ([5af0eae](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/5af0eae4e2eb30af9c3757e0c08bf32cf31b50c9))
* **radicale:** add new env parameter ([dad82db](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/dad82dba6227cd804d0371d22198168a80401deb))
* update app version and subcharts dependencies ([62e4b58](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/62e4b586df618a3a101e7d9e2cc8876a3bcfe5a0))
* update blog subchart ([bf1eab1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/bf1eab1d479f26a3765e1168d42483af9ef06967))
* use chart.yaml as default version ([4fc81b2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/4fc81b2ea2a46dec26afc07eb23cb903b63937cd))
* use chart.yaml as default version ([7bffc23](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/7bffc2304543bdb5078fdffb10bad59af56edfb2))


### Bug Fixes

* **laboite:** set new subcharts dependencies ([2a565f8](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/2a565f8c5ef0e54386e7d5f5dcd27f7ae394c8a0))
* **laboite:** update addons new version ([bfe91dd](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/bfe91dd30d35994918f0fd8d0b0d37db9d9971c0))
* set correct appversion ([0f62878](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/0f628783da2ac439463120164bf0259d9994d2a4))
* update hpa api version removed on k8s 1.26 ([23ea31b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/23ea31bfc26c4418f57c6084edac2655d0807d1c))
* **values:** add nextcloud groupurl parameter in meteor-settings ([74269c5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/74269c5701fb4e881076ca67bed9bc06c1e9157d))
* **values:** add nextcloud values for user, password and quota ([b6ca9aa](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/b6ca9aa03f4c6fa7b1be38bbcf683638a4dd1029))


### Continuous Integration

* **commitlint:** enforce commit message format ([f7f4ebb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/f7f4ebb0588bf2715cb14177ae334a1748d830de))
* **release:** create release automatically with `semantic-release` ([7f174b0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/7f174b0762d409d4580a5bc427150912d5e438f1))
